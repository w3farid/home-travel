# Spring Boot + Spring Security + OAuth2 example

## Getting started
- Java 8
- Maven
- PostgreSQL (create database name like as home-travel or change properties file)
- go to project root directory and run command : `mvn clean install` 
- then run command : `mvn spring-boot:run`
- go to your browser set url : `http://localhost:8080/`
- or use postman tools `http://localhsot:8080/auth/token` and follow others

- port change set in properties file: `server.port= 8080`


It is possible to run application in one of two profiles:
- h2
- postgres

depending on database engine chose for testing. 

To enable cache statistics `dev` profile needs to be turned on.

### Testing database schema
![database-schema](src/main/docs/Selection_003.png)

### Authentication
![database-schema](src/main/docs/Selection_002.png)
```

-- url: `http://localhost:8080/oauth/token`

-- `grant_type=password`

-- `username=admin`

-- `password=admin1234`

-- `client_id=spring-security-oauth2-read-write-client`

### basic auth
-- username: `spring-security-oauth2-read-write-client`
-- password: `spring-security-oauth2-read-write-client-password1234`


### Accessing secured company 
##### get company data: 

-- `http://localhost:8080/secured/company`

-- `authorization: Bearer 6a55280f-8e18-4bb8-a602-bce7b8ee9ff7`

