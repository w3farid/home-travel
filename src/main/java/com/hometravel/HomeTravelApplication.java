package com.hometravel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeTravelApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeTravelApplication.class, args);
	}
}
